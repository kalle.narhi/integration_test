const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex)
} 

module.exports = {
    rgbToHex: (r, g, b) => {
        const redHex = r.toString(16);
        const greenHex = g.toString(16);
        const blueHex = b.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex);
    },
    hexToRgb: (hex) => {
        const red = parseInt(hex.slice(0,2), 16);
        const green = parseInt(hex.slice(2,4), 16);
        const blue = parseInt(hex.slice(4,6), 16);

        return red + "," + green + "," + blue;
    }
}