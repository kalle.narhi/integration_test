// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("RGB to Hex conversion", () => {
        it("converts the basic colors", () => {
            const redHex = converter.rgbToHex(255, 0, 0); // ff0000
            const greenHex = converter.rgbToHex(0, 255, 0); // 00ff00
            const blueHex = converter.rgbToHex(0, 0, 255); // 0000ff

            expect(redHex).to.equal("ff0000");
            expect(blueHex).to.equal("0000ff");
            expect(greenHex).to.equal("00ff00");
        });
    }),
    describe("Hex to RGB", () => {
        it("converts hex to red, green and blue", () => {
            const red = converter.hexToRgb("ff0000"); // 255, 0, 0
            const green = converter.hexToRgb("00ff00"); // 0, 255, 0
            const blue = converter.hexToRgb("0000ff"); // 0, 0, 255

            expect(red).to.equal("255,0,0");
            expect(green).to.equal("0,255,0");
            expect(blue).to.equal("0,0,255");
        });
    });
});